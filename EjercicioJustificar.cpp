#include <stdio.h>
#include <stdlib.h>

/*int suma (int op1, int op2);*/
/*La declaración, es el tipo de dato de una función que se compone de el tipo de valor de retorno y el tipo de valor de los parámetros*/
/*Las variables declaradas fuera del main se pueden llegar a usar en todo el programa, es decir, es una variable global*/

int op;

int suma (int op1, int op2) {

        return op1 + op2;
}

int resta (int op1, int op2) {

        return op1 - op2;
}
int multi (int op1, int op2) {

        return op1 * op2;
}
int preguntar_op () {
        static int nop = 0; /*static... sirve para conservar el valor de una variable local en todo el programa*/
        int op;
printf("Operando %i: ", ++nop);
scanf("%i", &op);

        return op;
}

int
main (){
                /* Variables*/
                int op1,op2,resultsuma,resultresta,resultmulti;		
                system ("clear"); //Limpiar la terminal
                system ("toilet -fpagga --gay JUSTIFY"); //Poner un diseño al principio del programa
                printf("\n\n");

                /*ENTRADA DE DATOS*/
                op1 = preguntar_op ();
                op2 = preguntar_op ();
/*ALGORITMO*/
                resultsuma = suma (op1,op2);
                resultresta = resta (op1,op2);
                resultmulti = multi (op1,op2);

                /*SALIDA DE DATOS*/
                printf("%i + %i = %6i \n",op1,op2,resultsuma);
                printf("%i - %i = %07i \n",op1,op2,resultresta);
                printf("%i * %i = \t %07i \n",op1,op2,resultmulti);
                return EXIT_SUCCESS;
}
