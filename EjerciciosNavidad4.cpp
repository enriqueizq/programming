#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

int
main(void){

        int num;

        printf("Escribe un número por favor: \n ");
        scanf ("%i", &num);
        printf("Su número en hexadecimal es: %x \n ", num);
        printf("Y la direccion de memoria de %i es %p \n", num, &num);
        return EXIT_SUCCESS;
}