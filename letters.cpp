#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
#define MAX 0x20

int main (int argc, char *argv[]) {

    char car = 'B';

    printf ("Caracter: %c\n", car);
    printf ("Caracter: %c\n", 'B');
    printf ("Caracter: %c\n", 0x42);

    printf ("\n");

    printf ("Cadena 1\n");
    printf ("Cadena 1 Cadena 2\n");
    printf ("Cadena 1"
            " "      "Cadena 2\n");

    printf ("\n");

    printf (
    "Usage:     letters [options]\n"
    "Prints letters.\n"
    );

    printf ("\n");

    printf ("\
            Usage:     letters [options]  \n\
            Prints Characters.            \n\
                                          \n\
            Options:                      \n\
");


    printf ("\n");

    printf ("Nombre: %s\n", "Txema");

    const char * name = "Txema";
    printf ("Nombre: %s\n", name);

	char mi_nombre[MAX];
	char hexa [MAX];
	printf ("Hexa: ");
	scanf (" %8[0-9a-fA-F]", hexa);/*CONJUNTO DE SELECCIÓN*/
	printf ("Hexa: %s\n", hexa);
	__fpurge (stdin);

	printf("Nombre: ");
	scanf(" %[^\n]", mi_nombre); /*CONJUNTO DE SELECCIÓN INVERSO*/
	printf("Nombre: %s\n", mi_nombre);
	__fpurge (stdin);


    return EXIT_SUCCESS;
}

