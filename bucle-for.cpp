#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

int main(){

    /*VARIABLES*/

	char asterisco = '*';
    char character;
	int num = 0;
    char character_user;
    int cantidad_numeros;
	int numeros;
	int incrementar = 0;
	int sumar = 0;
	double media = 0.00;
	double numeros_media;
	int i;

	for(i = 0; i<11; i++){
	printf("%i \n", i);
	}

	for(int i = 0; i<10; i++){
	printf("%c \n", asterisco);
	}

	printf("Introduce un caracter: ");
	scanf("%c", &character);
	

	for(int i = 1; i<=character; i++){ // ESTO CONSIGUE QUE TENGA EL ASCII DEL CARACTER DEL USUARIO Y EL CHAR Y NOS RECOGE EL ASCII DE ESE NUMERO
	printf("%c = %i \n", character, num++); //DEFINO NUM++ PARA QUE INCREMENTE CADA VEZ QUE SE AÑADA UN CARACTER NUEVO
    }


	printf("Introduce una letra o un numero: ");
	scanf("%c", &character_user);

	for (int i = 0; i< 11; i++){
	printf("%c \n", character_user);
	}
printf("Indique los número para hacer la media: ");
	scanf("%i", &cantidad_numeros); //PREGUNTAMOS AL USUARIO LA CANTIDAD DE NUMEROS QUE NECESITA Y LO ALMACENAMOS EN LA VARIABLE

	for(int i = 0; i<cantidad_numeros; i++){ // ESTO CONSIGUE QUE SOLO TENGA LOS NUMEROS QUE EL USUARIO DIGA
	printf("Introduce su %iº numero: ", ++incrementar); // AQUI INCREMENTO UN NUMERO PARA QUE HAGA UN SEGUIMIENTO DE TODOS LOS NUMEROS QUE EL
							    // USUARIO QUIERE
	scanf("%i", &numeros); 
	sumar = sumar + numeros; 
	}
	numeros_media = cantidad_numeros; // PASAMOS LA CANTIDAD DE NUMERO A VARIABLE DE TIPO FLOAT PARA QUE PODAMOS TENER DECIMALES

	media = sumar / numeros_media; //HACEMOS LA OPERACION DE LA MEDIA
	printf("La media es: %.2lf \n", media); // IMPRIMIMOS CON SOLO 2 DECIMALES

	return EXIT_SUCCESS;

}

	