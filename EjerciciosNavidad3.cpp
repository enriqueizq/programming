#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

int op;

int suma (int op1, int op2) {

	return op1 + op2; 
}

int preguntar_op () {
	static int nop = 0; 
	int op;
printf("Operando %i: ", ++nop);
scanf("%i", &op);

	return op;
}


int 
main(void)
{
        /*VARIABLES*/
            int op1,op2,result;
            const char *const azulclaro = "\033[1;40;34m";
            const char *const normal = "\033[0m";
            const char *const verde = "\033[0;40;32m";
            /*ENTRADA DE DATOS*/ 
		op1 = preguntar_op ();
		op2 = preguntar_op ();

        /*ALGORITMIA*/
        result = suma (op1,op2);

        /*SALIDAD DE DATOS*/
        printf("la suma de %s%i%s y %s%i%s es: %s%i%s", azulclaro, op1, normal, azulclaro, op2, normal, verde, result, normal);

        return EXIT_SUCCESS;

        
}