#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

int 
main (){

     /*VARIABLES*/  
        int num1;
        float num2;
        int num3;
        char charValue = num3+'0';
    /*SALIDA DE DATOS*/

        printf("Escriba un número entero por favor: \n ");
        scanf("%i", &num1);
        printf("Su número entero es: %i \n ", num1);

        printf ("Escriba ahora un número entero por favor: \n ");
        scanf("%f", &num2);
        printf("Su número real es: %f \n ", num2);
        
        printf ("Escriba por favor un número entre 32 y 128: \n ");
        scanf("%i", &num3);
        printf ("Su número es %i y su caracter relacionado es %c \n ", num3, charValue);
        
        return EXIT_SUCCESS;
}