
#include <stdio.h> //líbrerias obligatoria para programar en C
#include <stdlib.h> //librería obligatoria
//lo anterior es para hacer más legible el programa, además de ser una constante simbólica.
int main () { // Declarar el cuerpo del programa

printf ("Hola mundo\n"); /* Imprimir en pantalla
							"\n" nos sirve para meter un salto de línea 
							Formato del printf --> printf("<<texto a meter");*/

																				//'a' las comillas simples sirven para escribir caracter
																				//"a"->'a',00 las comillas dobles sirven para escribir cadenas de caracteres

return EXIT_SUCCESS; //Terminar el cuerpo del programa 

}