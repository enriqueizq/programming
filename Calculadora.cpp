#include <stdio.h>
#include <stdlib.h>

/*int suma (int op1, int op2);*/ 
/*La declaración, es el tipo de dato de una función que se compone de el tipo de valor de retorno y el tipo de valor de los parámetros*/
/*Las variables declaradas fuera del main se pueden llegar a usar en todo el programa, es decir, es una variable global*/

int op;

int suma (int op1, int op2) {

	return op1 + op2; 
}

int resta (int op1, int op2) {

	return op1 - op2;
}
int multi (int op1, int op2) {
	
	return op1 * op2;
}

int preguntar_op () {
	static int nop = 0; /*static... sirve para conservar el valor de una variable local en todo el programa*/
	int op;
printf("Operando %i: ", ++nop);
scanf("%i", &op);

	return op;
}

int 
main (){
		/* Variables*/
		int op1,op2,resultsuma,resultresta,resultmulti;

		system ("clear"); //Limpiar la terminal
		system ("toilet -fpagga --gay CALCULADORA"); //Poner un diseño al principio del programa
		printf("\n\n");

		/*ENTRADA DE DATOS*/ 
		op1 = preguntar_op ();
		op2 = preguntar_op ();
		
		/*ALGORITMO*/
		resultsuma = suma (op1,op2);
		resultresta = resta (op1,op2);
		resultmulti = multi (op1,op2);

		/*SALIDA DE DATOS*/
		printf("%i + %i = %i \n",op1,op2,resultsuma);
		printf("%i - %i = %i \n",op1,op2,resultresta);
		printf("%i * %i = %i \n",op1,op2,resultmulti);
		system ("figlet algunos datos"); //Imprimir una frase en pantalla con un diseño "figlet"
		printf("\n\n");
		printf ("&%p: %i(%lu bytes)\n", &op1,op1,sizeof (op1) );
	       	printf ("&%p: %i(%lu bytes)\n", &op2,op2,sizeof (op2) );
		printf ("las direcciones de memoria ocupan: %lu bytes.\n", sizeof (&op1));
		printf ("el tipo de datos para guardar la dirección de op1 es int *\n");


		return EXIT_SUCCESS;
	}
