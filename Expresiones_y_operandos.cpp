#include <stdio.h>
#include <stdlib.h>
#define MAX 10


void primer_ejercicio(){

	int primer_array[MAX];
	
	printf("La cantidad del array es de: %li bytes\n", sizeof(primer_array));
}


void segundo_ejercicio(){
	int segundo_array[MAX];
	int tamaño_elemento, tipo_dato, cantidad_elementos;

	tamaño_elemento = sizeof(segundo_array); 		//CANTIDAD DE BYTES QUE OCUPA EL ARRAY
	tipo_dato = sizeof(int); 						// CUANTO OCUPA UN VALOR INT SE PUEDE CAMBIAR 
	
	cantidad_elementos = tamaño_elemento / tipo_dato; 
	//DIVIDIMOS LOS BYTES TAMAÑO_ELEMENTO / TIPO DATO Y ASI PODEMOS SABER CUANTOS ELEMENTOS TIENE EL ARRAY.
       		//SI QUEREMOS VER LOS ELEMENTOS EN TIPO LONG INT ES TAN SIMPLE COMO CAMBIAR EL TIPO DE DATO Y PONER EN VEZ DE INT LONG INT

	printf("La cantidad de elementos de una array es de: %i \n\n", cantidad_elementos);
	
	
}



void tercer_ejercicio(){
	int a;
	long int b;
	long long int c;

	printf("El tamaño de una variable de tipo int es de %li \n", sizeof(a));
	printf("El tamaño de una variable de tipo long int es de %li \n", sizeof(b));
	printf("El tamaño de una variable de tipo long long int es de %li \n",  sizeof (c));


	
}
int main(){

	primer_ejercicio();
	
	segundo_ejercicio();

	tercer_ejercicio();
    
	return EXIT_SUCCESS;

}

