#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
    int i; 

    for(i=0;i<100000000000;i++){ 
    if(i%300 == 0){ //SI EL RESTO i dividido entre 300 ES 0
    	printf("\033[L");
    	printf("*"); 
    	}

    if(i%400 == 0){ // SI EL RESTO DE 400 ES 0
    	printf("\033[2J"); //BORRA PANTALLA
    	}
    }
    return EXIT_SUCCESS;
}
